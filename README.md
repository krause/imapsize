#### What
Analyze imap mailbox folder sizes.

Careful though, this script will fetch **all** of your emails to work on servers not supporting the QUOTA feature.

#### Setup

```
mkvirtualenv -p $(which python3) imapsize
pip install -r requirements.txt
```

#### How

```
workon imapsize
python imapsize.py mail.mpib-berlin.mpg.de
```
