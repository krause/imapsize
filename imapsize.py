from imapclient import IMAPClient
import argparse
from getpass import getpass
import sys
from itertools import zip_longest


class Fatal(Exception):
    pass


class InvalidSize(Exception):
    pass


class FolderStat(object):

    size = 0

    def __init__(self, id):
        self.id = id
        self.name = str(id)
        self.invalid = False

    @property
    def scaled(self):
        return _scale_bytes(self.size)

    def __unicode__(self):
        return '{}: {}'.format(self.name, _scale_bytes(self.size))


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def _scale_bytes(bytecount):
    byte_s = '{}B'.format(bytecount)
    for suffix in ['K', 'M', 'G', 'T']:
        if bytecount > 1024:
            bytecount = bytecount / 1024
            byte_s = '{:<10.3f} {}B'.format(bytecount, suffix)
    return byte_s


def connect(args):
    # we don't even bother with non-ssl non-standard ports here - fix your shit
    try:
        client = IMAPClient(args.server, ssl=True)
        client.login(args.username, args.password)
    except Exception as e:
        raise Fatal('Connection Error: {}'.format(e))
    return client


def _get_size(client, folder):
    msg_ids = client.search()
    size = 0
    for chunk in grouper(msg_ids, 1000, None):
        chunk = [x for x in chunk if x is not None]
        try:
            msgs = client.fetch(chunk, data=['RFC822.SIZE'])
        except Exception as e:
            raise InvalidSize('imaplib error: {}'.format(str(e)))
        for m_data in msgs.values():
            try:
                size += m_data[b'RFC822.SIZE']
            except KeyError:
                pass
    return size


def scan(client, args):
    folders = client.list_folders()
    stats = []
    for count, folder in enumerate(folders):
        print('Scanning folders... {:>3}/{}'.format(
            count, len(folders)), end='\r')
        folder = FolderStat(folder[2])
        client.select_folder(folder.id)
        try:
            folder.size = _get_size(client, folder.id)
        except InvalidSize as e:
            print(e)
            folder.size = 0
            folder.invalid = True
            client = connect(args)
            continue
        stats.append(folder)
    print()
    return stats


def summary(stats, sort=True, scale=True):
    print()
    print('-------- Summary ---------')
    print()
    fullsize = 0
    for folder in stats:
        fullsize += folder.size
    if sort:
        stats = sorted(stats, key=lambda folder: folder.size, reverse=True)
    for folder in stats:
        if scale:
            value = folder.scaled
        elif folder.invalid:
            value = 'Could not scan'
        else:
            value = str(folder.size)
        print("{:<50} : {}".format(folder.name, value))
    if scale:
        value = _scale_bytes(fullsize)
    else:
        value = fullsize
    print()
    print('{:<50} : {}'.format('Entire Mailbox', value))


def main():
    parser = argparse.ArgumentParser(
        description='Get IMAP mailbox size stats.')
    parser.add_argument('server', type=str,
                        help='hostname for the mail server')
    args = parser.parse_args()

    print('Okay, trying to login to: {}'.format(args.server))
    setattr(args, 'username', input('Username: '))
    setattr(args, 'password', getpass())

    try:
        client = connect(args)
        sizes = scan(client, args)
        summary(sizes)
    except Fatal as e:
        print(e)
        sys.exit(1)


if __name__ == '__main__':
    main()
